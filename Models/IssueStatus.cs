﻿namespace BugTracker.Models
{
    public class IssueStatus
    {
        public Status Status { get; set; }
    }

    public enum Status
    {
        Todo,
        Progressing,
        Done,
        Cancelled,
        Blocked
    }
}
