﻿using System;
namespace BugTracker.Models
{
    public class Comment
    {
        public string Text { get; set; }
        public DateTime DateTimePosted { get; set; }
        public User User { get; set; }
    }
}