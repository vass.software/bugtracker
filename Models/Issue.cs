﻿using System;
using System.Collections.Generic;

namespace BugTracker.Models
{
    public class Issue
    {
        public String Summary { get; set; }
        public String? Description { get; set; }
        public Byte? StoryPoints { get; set; }
        public List<Comment>? Comments { get; set; }
        public IssueStatus Status { get; set; }
    }

}
